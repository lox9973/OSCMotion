# OSCMotion

OSCMotion is a tool to stream avatar motion from Unity to VRChat by sending muscle values over OSC. It can be easily installed on any VRC 3.0 avatars by setting up an animator controller. The motion is synchronized as animator parameters, and therefore it can be seen by other players.

Combined with other full body tracking tools, you can use full body tracking in desktop mode without SteamVR or any headset!

Please check the [wiki](../../wikis/home) for quick start.
