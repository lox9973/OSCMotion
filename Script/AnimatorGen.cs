#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using Array = System.Array;
using UnityEditor.Animations;

namespace OSCMotion {
public class AnimatorGen {
	const string ParamEps = "_Eps";
	const string ParamOne = "_One";
	const string ParamEnabled = "OSCMotion";
	const string TypeNameVRCExpressionParameters = "VRC.SDK3.Avatars.ScriptableObjects.VRCExpressionParameters, VRCSDK3A";
	static BlendTree AddDirectBlend(AnimatorController controller, int layerIndex) {
		var state = controller.CreateBlendTreeInController("Blend", out var blendTree, layerIndex);
		state.writeDefaultValues = false;
		blendTree.blendType = BlendTreeType.Direct;
		return blendTree;
	}
	static void AddChildMotion(BlendTree blendTree, string param, params (string prop, float value)[] motions) {
		var clip = new AnimationClip();
		BakeIntoPoseBasedUponOrigin(clip);
		clip.name = param;
		foreach(var motion in motions)
			clip.SetCurve("", typeof(Animator), motion.prop, new AnimationCurve(new Keyframe(0, motion.value)));
		AssetDatabase.AddObjectToAsset(clip, AssetDatabase.GetAssetPath(blendTree));

		blendTree.AddChild(clip);
		var children = blendTree.children;
		children[children.Length-1].directBlendParameter = param;
		blendTree.children = children;
	}
	static void AddChildDriver(BlendTree blendTree, string param, float scale) {
		var childTree = blendTree.CreateBlendTreeChild(0);
		childTree.blendType = BlendTreeType.Simple1D;
		childTree.blendParameter = param;
		childTree.useAutomaticThresholds = false;
		var children = blendTree.children;
		children[children.Length-1].directBlendParameter = ParamOne;
		blendTree.children = children;

		var path = AssetDatabase.GetAssetPath(blendTree);
		for(int x=-1; x<=1; x+=2) {
			var clip = new AnimationClip();
			clip.name = $"{param}@{x}";
			clip.SetCurve("", typeof(Animator), $"{param}-", new AnimationCurve(new Keyframe(0, -x * scale)));
			clip.SetCurve("", typeof(Animator), $"{param}+", new AnimationCurve(new Keyframe(0, +x * scale)));
			AssetDatabase.AddObjectToAsset(clip, path);
			childTree.AddChild(clip, x);
		}
	}
	[MenuItem("OSCMotion/GenerateAction")]
	static void GenerateAction() {
		var path = "Assets/OSCMotion/OMAction.controller";
		AssetDatabase.DeleteAsset(path);
		var controller = AnimatorController.CreateAnimatorControllerAtPath(path);

		const float eps = 1e-6f; // avoid sum-of-weights clamping in blendMotion

		controller.AddParameter(ParamEps, AnimatorControllerParameterType.Float);
		controller.AddParameter(ParamOne, AnimatorControllerParameterType.Float);
		foreach(var param in Standard.Parameters) {
			controller.AddParameter($"{param.paramName}", AnimatorControllerParameterType.Float);
			controller.AddParameter($"{param.paramName}+", AnimatorControllerParameterType.Float);
			controller.AddParameter($"{param.paramName}-", AnimatorControllerParameterType.Float);
		}
		var parameters = controller.parameters;
		parameters[0].defaultFloat = eps;
		parameters[1].defaultFloat = 1f;
		controller.parameters = parameters;

		controller.AddLayer("Driver");
		var layers = controller.layers;
		layers[1].defaultWeight = 1f;
		controller.layers = layers;

		var blendMotion = AddDirectBlend(controller, 0);
		var blendDriver = AddDirectBlend(controller, 1);

		AddChildMotion(blendMotion, ParamEps, Standard.Parameters
			.SelectMany(p => p.motions.Select(m => (m.muscle, 0f))).ToArray());
		foreach(var param in Standard.Parameters) {
			AddChildDriver(blendDriver, param.paramName, param.mainMuscle.StartsWith("RootQ.") ? 1f : eps);
			if(param.mainMuscle.StartsWith("RootT.")) {
				AddChildMotion(blendMotion, $"{param.paramName}-", (param.mainMuscle, -Standard.PositionScale/eps));
				AddChildMotion(blendMotion, $"{param.paramName}+", (param.mainMuscle, +Standard.PositionScale/eps));
			} else if(param.mainMuscle.StartsWith("RootQ.")) {
				AddChildMotion(blendMotion, $"{param.paramName}-", (param.mainMuscle, -1f), ("RootQ.w", 0f));
				AddChildMotion(blendMotion, $"{param.paramName}+", (param.mainMuscle, +1f), ("RootQ.w", 0f));
			} else {
				AddChildMotion(blendMotion, $"{param.paramName}-",
					Array.ConvertAll(param.motions, m => (m.muscle, m.weight/eps * 
						180f/HumanTrait.GetMuscleDefaultMin(Array.IndexOf(HumanTrait.MuscleName, m.muscle)))));
				AddChildMotion(blendMotion, $"{param.paramName}+",
					Array.ConvertAll(param.motions, m => (m.muscle, m.weight/eps * 
						180f/HumanTrait.GetMuscleDefaultMax(Array.IndexOf(HumanTrait.MuscleName, m.muscle)))));
			}
		}
		
		AssetDatabase.SaveAssets();
	}
	[MenuItem("OSCMotion/GenerateParam")]
	static void GenerateParam() {
		var path = "Assets/OSCMotion/OMParam.asset";
		AssetDatabase.DeleteAsset(path);
		var param = ScriptableObject.CreateInstance(System.Type.GetType(TypeNameVRCExpressionParameters));
		AssetDatabase.CreateAsset(param, path);

		var so = new SerializedObject(param);
		var parameters = so.FindProperty("parameters");
		parameters.ClearArray();
		for(int i=0; i<=Standard.Parameters.Length; i++) {
			parameters.InsertArrayElementAtIndex(i);
			var parameter = parameters.GetArrayElementAtIndex(i);
			var name = parameter.FindPropertyRelative("name");
			var valueType = parameter.FindPropertyRelative("valueType");
			var defaultValue = parameter.FindPropertyRelative("defaultValue");
			var saved = parameter.FindPropertyRelative("saved");
			defaultValue.floatValue = 0;
			saved.boolValue = false;
			if(i < Standard.Parameters.Length) {
				name.stringValue = Standard.Parameters[i].paramName;
				valueType.enumValueIndex = 1;
			} else {
				name.stringValue = ParamEnabled;
				valueType.enumValueIndex = 2;
			}
		}
		so.ApplyModifiedProperties();

		AssetDatabase.SaveAssets();
	}

	// static void PrintQuat(Quaternion q) {
	// 	Debug.Log($"{q.x}, {q.y}, {q.z}, {q.w}, {q.x*q.x+q.y*q.y+q.z*q.z+q.w*q.w}");
	// }
	// [MenuItem("OSCMotion/TestRootQ")]
	// static void TestRootQ() {
	// 	var go = Selection.activeGameObject;
	// 	var animator = go.GetComponent<Animator>();
	// 	var humanPoseHandler = new HumanPoseHandler(animator.avatar, animator.transform);
	// 	var humanPose= new HumanPose();
	// 	humanPoseHandler.GetHumanPose(ref humanPose);
	// 	PrintQuat(humanPose.bodyRotation);
	// 	// var hips = animator.GetBoneTransform(HumanBodyBones.Hips);
	// 	// PrintQuat(hips.localRotation);
	// }
	// [MenuItem("OSCMotion/PrintMuscle")]
	// static void PrintMuscle() {
	// 	var s = "";
	// 	for(var i=0; i<HumanTrait.MuscleName.Length; i++) {
	// 		s += $"{i}: {HumanTrait.MuscleName[i]}\n";
	// 	}
	// 	Debug.Log(s);
	// }

	// copied from ShaderMotion
	static void BakeIntoPoseBasedUponOrigin(AnimationClip clip) {
		var settings = AnimationUtility.GetAnimationClipSettings(clip);
		settings.loopBlendOrientation    = true; // Root Transform Rotation: Bake Into Pose
		settings.keepOriginalOrientation = true; // Root Transform Rotation: Based Upon = Origin
		settings.orientationOffsetY      = 0;    // Root Transform Rotation: Offset
		settings.loopBlendPositionY      = true; // Root Transform Position (Y): Bake Into Pose
		settings.keepOriginalPositionY   = true; // Root Transform Position (Y): Based Upon = Origin
		settings.level                   = 0;    // Root Transform Position (Y): Offset
		settings.loopBlendPositionXZ     = true; // Root Transform Position (XZ): Bake Into Pose
		settings.keepOriginalPositionXZ  = true; // Root Transform Position (XZ): Based Upon = Origin
		AnimationUtility.SetAnimationClipSettings(clip, settings);
	}
}
}
#endif