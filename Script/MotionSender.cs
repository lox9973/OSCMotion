using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Array = System.Array;
using OscCore;

namespace OSCMotion {
public class MotionSender: MonoBehaviour {
[Header("Motion")]
	public Animator animator;
	public Transform origin;
	public bool mirror = false;

[Header("Network")]
	public string ipAddress = "127.0.0.1";
	public int port = 9000;
	public int updateRate = 30;

	private OscClient client;
	private float elapsed;
	void OnEnable() {
		humanPoseHandler = new HumanPoseHandler(animator.avatar, animator.transform);
		if(client == null)
			client = new OscClient(ipAddress, port);
		elapsed = 0f;
	}
	void SendFloat(string addr, float value) {
		client.Writer.Reset();
		client.Send(addr, value);
	}
	void Update() {
		elapsed += Time.deltaTime;
		if(elapsed*updateRate > 0.5f) {
			var deltaTime = 1f/updateRate;
			elapsed -= deltaTime;
			if(elapsed >= deltaTime)
				elapsed = 0f;
			SendPose();
		}
	}

	private Vector3 rootT;
	private Quaternion rootQ;
	private float[] muscleDegrees;
	void SendPose() {
		GetPose();

		var sendT = rootT/Standard.PositionScale;
		// parameter (a,b,c) produces quaternion (a,b,c,1-a-b-c)
		var sendQ = new Vector4(rootQ.x, rootQ.y, rootQ.z, rootQ.w) / ((rootQ.w > 0f ? +1f : -1f) *
			(Mathf.Abs(rootQ.x) + Mathf.Abs(rootQ.y) + Mathf.Abs(rootQ.z) + Mathf.Abs(rootQ.w)));

		if(mirror) {
			sendT.x = -sendT.x;
			sendQ.y = -sendQ.y;
			sendQ.z = -sendQ.z;
		}
		SendFloat($"/avatar/parameters/RootTx", sendT.x);
		SendFloat($"/avatar/parameters/RootTy", sendT.y);
		SendFloat($"/avatar/parameters/RootTz", sendT.z);
		SendFloat($"/avatar/parameters/RootQx", sendQ.x);
		SendFloat($"/avatar/parameters/RootQy", sendQ.y);
		SendFloat($"/avatar/parameters/RootQz", sendQ.z);

		for(int i=6; i<Standard.Parameters.Length; i++) {
			var param = Standard.Parameters[i];
			var value = 0f;
			foreach(var id in param.muscleIds)
				value += muscleDegrees[id];
			value /= 180f;

			if(mirror && param.paramNameMirror == null)
				value = -value;
			if(mirror && param.paramNameMirror != null)
				SendFloat($"/avatar/parameters/{param.paramNameMirror}", value);
			else
				SendFloat($"/avatar/parameters/{param.paramName}", value);
		}
	}
	private float[] MuscleDefaultMax;
	private float[] MuscleDefaultMin;
	private HumanPoseHandler humanPoseHandler;
	private HumanPose humanPose;
	void GetPose() {
		if(muscleDegrees == null) {
			muscleDegrees = new float[HumanTrait.MuscleCount];
			MuscleDefaultMax = Enumerable.Range(0, muscleDegrees.Length).Select(i => HumanTrait.GetMuscleDefaultMax(i)).ToArray();
			MuscleDefaultMin = Enumerable.Range(0, muscleDegrees.Length).Select(i => HumanTrait.GetMuscleDefaultMin(i)).ToArray();
		}
		humanPoseHandler.GetHumanPose(ref humanPose);
		(rootT, rootQ) = GetRootMotion(ref humanPose, animator);
		if(origin) {
			var q = Quaternion.Inverse(origin.rotation) * animator.transform.rotation;
			rootQ = q * rootQ;
			rootT = q * (rootT - animator.transform.InverseTransformPoint(origin.position) / animator.humanScale);
		}
		for(int i=0; i<muscleDegrees.Length; i++) {
			muscleDegrees[i] = humanPose.muscles[i];
			muscleDegrees[i] *= muscleDegrees[i] >= 0f ? MuscleDefaultMax[i] : -MuscleDefaultMin[i];
		}
	}
	// edited from ShaderMotion
	static (Vector3, Quaternion) GetRootMotion(ref HumanPose pose, Animator animator) {
		var anim = animator.transform;
		// bodyPosition/bodyRotation represents the center of mass relative to animator parent, divided by the human scale
		// anim.localScale seems ignored
		var rootQ = Quaternion.Inverse(anim.localRotation) * pose.bodyRotation;
		var rootT = Quaternion.Inverse(anim.localRotation) * (pose.bodyPosition-anim.localPosition/animator.humanScale);
		// Unity <=2019 seems to ignore localScale on the open interval (hips, anim) as well
#if !UNITY_2020_1_OR_NEWER // TODO: tested on 2021 but not 2020
		var hips = animator.GetBoneTransform(HumanBodyBones.Hips);
		rootT = anim.InverseTransformPoint(hips.parent.TransformPoint(
			InverseTransformPointTQ(rootT*animator.humanScale, hips.parent, anim)))/animator.humanScale;
#endif
		return (rootT, rootQ);
	}
	static Vector3 InverseTransformPointTQ(Vector3 pos, Transform begin, Transform end) {
		var chainT = Vector3.zero;
		var chainQ = Quaternion.identity;
		for(var x = begin; x != end; x = x.parent) {
			chainT = x.localRotation * chainT + x.localPosition;
			chainQ = x.localRotation * chainQ;
		}
		return Quaternion.Inverse(chainQ) * (pos - chainT);
	}
}
}