using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Array = System.Array;

namespace OSCMotion {
public class Parameter {
	public string paramName, paramNameMirror;
	public (string muscle, float weight)[] motions;
	public int[] muscleIds;
	public Parameter(string mainMuscle, params (string muscle, float weight)[] subMuscles) {
		this.paramName = Regex.Replace(mainMuscle, @"\W", "");
		this.paramNameMirror = mirrorParamName(this.paramName);
		this.motions = subMuscles.Prepend((mainMuscle, 1-subMuscles.Sum(x => x.weight))).ToArray();
		this.muscleIds = Array.ConvertAll(this.motions, m => Array.IndexOf(HumanTrait.MuscleName, m.muscle));
	}
	public string mainMuscle => this.motions[0].muscle;
	static string mirrorParamName(string name) {
		if(name.StartsWith("Left"))
			return $"Right{name.Substring(4)}";
		else if(name.StartsWith("Right"))
			return $"Left{name.Substring(5)}";
		else if(!name.EndsWith("LeftRight"))
			return name;
		return null;
	}
}
public static class Standard {
	public const float PositionScale = 2f;
	public static Parameter[] Parameters = new []{
		new Parameter("RootT.x"),
		new Parameter("RootT.y"),
		new Parameter("RootT.z"),
		new Parameter("RootQ.x"),
		new Parameter("RootQ.y"),
		new Parameter("RootQ.z"),

		new Parameter("Spine Front-Back"),
		new Parameter("Spine Left-Right"),
		new Parameter("Spine Twist Left-Right"),
		new Parameter("Chest Front-Back",       ("UpperChest Front-Back", 0)),
		new Parameter("Chest Left-Right",       ("UpperChest Left-Right", 0)),
		new Parameter("Chest Twist Left-Right", ("UpperChest Twist Left-Right", 0)),

		new Parameter("Head Nod Down-Up",       ("Neck Nod Down-Up", 0f)),
		new Parameter("Head Tilt Left-Right",   ("Neck Tilt Left-Right", 0f)),
		new Parameter("Head Turn Left-Right",   ("Neck Turn Left-Right", 0f)),

		new Parameter("Left Arm Down-Up",       ("Left Shoulder Down-Up", 0)),
		new Parameter("Left Arm Front-Back",    ("Left Shoulder Front-Back", 0)),
		new Parameter("Left Arm Twist In-Out"),
		new Parameter("Left Forearm Stretch"),

		new Parameter("Right Arm Down-Up",      ("Right Shoulder Down-Up", 0)),
		new Parameter("Right Arm Front-Back",   ("Right Shoulder Front-Back", 0)),
		new Parameter("Right Arm Twist In-Out"),
		new Parameter("Right Forearm Stretch"),

		new Parameter("Left Upper Leg Front-Back"),
		new Parameter("Left Upper Leg In-Out"),
		new Parameter("Left Upper Leg Twist In-Out"),
		new Parameter("Left Lower Leg Stretch"),

		new Parameter("Right Upper Leg Front-Back"),
		new Parameter("Right Upper Leg In-Out"),
		new Parameter("Right Upper Leg Twist In-Out"),
		new Parameter("Right Lower Leg Stretch"),
	};
}
}